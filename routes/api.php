<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::group([
    'prefix'    => 'auth',
    'namespace' => 'API',
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::get('me', 'AuthController@me');
    Route::post('password/email', 'AuthController@sendResetLinkEmail');
    Route::post('password/reset', 'AuthController@resetPassword');
});

Route::post('/users', 'API\UserController@store')->name('users.store');

Route::group([
    'middleware' => 'auth:api',
    'namespace'  => 'API',
], function () {
    Route::resource('users', 'UserController')->except([
        'index', 'store', 'create', 'edit',
    ]);
});
