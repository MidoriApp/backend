<?php

namespace App\Traits;

trait Validatable
{
    public static function getCreateRules()
    {
        $rules = isset(static::$createRules) ? static::$createRules : [];

        return $rules;
    }

    public static function getUpdateRules($model)
    {
        $rules = isset(static::$updateRules) ? static::$updateRules : [];

        return static::transformRule($rules, $model);
    }

    private static function transformRule(array $rules, $model)
    {
        $search = ['%id%'];
        $replace = [$model->id];

        return str_replace($search, $replace, $rules);
    }
}
